export const texts = [
  "There are different types of texts and interactive exercises that practise the reading skills you need to do well in your studies, to get ahead at work and to communicate in English in your free time.",
  "The Smiths live in a house. They have a living room. They watch TV in the living room. The father cooks food in the kitchen. They eat in the dining room. The house has two bedrooms. They sleep in the bedrooms.",
  "We took lots of breaks and sat in cafes along the river Seine. The French food we ate was delicious. The wines were tasty, too. Steve`s favorite part of the vacation was the hotel breakfast.",
  "To be sure, numbers determine the time individuals will wake up in the morning, how much money employees earn per hour, what day of the year it is, and much, much more.",
  "Mice are what you call more than one mouse. He likes these animals because they are small. Birds live in cages too. George sees a parrot and a canary. He likes them all, but he doesn`t want to clean the cage.",
  "Not only did he take a tour of this spectacular stadium, but he also got to watch a Chicago Cubs game. In the stadium, Keith and the other fans cheered for the Cubs. Keith was happy that the Cubs won with a score of 5-4.",
  "A common beach destination is the Santa Monica Pier, which offers rides and attractions to its visitors. Here, Stephanie rode the iconic Ferris wheel, which offered her a spectacular view of the city and coast.",
];

export default { texts };

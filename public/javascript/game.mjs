import { createSocket } from "./socket/index.mjs";

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

createSocket(username);

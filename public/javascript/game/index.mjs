import { GAME_OVER } from "../socket/env.mjs";
import { 
  disableRoomButtons, 
  checkLetter, 
  handleOnWin,
  getHTMLElement, 
  setSessionValues, 
  setResults,
  successLetter, 
  refreshGame,
  untraceableButtons 
} from "../helpers/game.mjs";


export const handleGameOver = (userResults, playTime) => {
  const timer = getHTMLElement('#game-time');
  const textContainer = getHTMLElement('#text-container');

  sessionStorage.setItem('gameOver', true)

  if (timer) {
    timer.innerHTML = "";
    timer.style.display = "none";
  }
  
  if (textContainer) {
    textContainer.innerHTML = setResults(playTime, userResults);

    const resultButton = getHTMLElement('#quit-results-btn');

    if (resultButton) {
      resultButton.addEventListener('click', () => {
        refreshGame();
      })
    }
  }
}

const handleKeydownClick = (ev, room, letters, text, textContainer, socket) => {
  ev.preventDefault();

  if (!untraceableButtons.includes(ev.key)) {
    const index = Number(sessionStorage.getItem('index'));

    const newIndex = index + 1;
    const status = text[newIndex] ? checkLetter(letters, index, ev.key) : 'done';

    if (status === 'true') {
      successLetter(textContainer, text, letters, newIndex, room, socket);
    } else if (status === 'false') {
      const mistakes = Number(sessionStorage.getItem('mistake'));

      sessionStorage.setItem('mistake', mistakes + 1);
    } else if (status === 'done') {
      handleOnWin(text, room, textContainer, socket);
    }
  }
}

const startGame = (room, playTime, text, textContainer, socket) => {
  let counter = playTime;

  const gameTimeContainer = getHTMLElement('#game-time');
  const letters = text.split('');

  const keyDownListener = (ev) => handleKeydownClick(ev, room, letters, text, textContainer, socket);

  setSessionValues();

  document.addEventListener('keydown', keyDownListener);

  if (gameTimeContainer) {
    gameTimeContainer.style.display = 'block';
    gameTimeContainer.innerText = `${counter} seconds left`;
  } 

  const gameInterval = setInterval(() => {
    if (gameTimeContainer) {
      const gameOver = sessionStorage.getItem('gameOver');

      gameTimeContainer.innerText = `${counter - 1} seconds left`;
      counter -= 1;

      if (counter === 0 || gameOver) {
        document.removeEventListener('keydown', keyDownListener);
        clearInterval(gameInterval);

        if (!gameOver) {
          socket.emit(GAME_OVER, room)
        }
      }
    } else {
      document.removeEventListener('keydown', keyDownListener);

      clearInterval(gameInterval);
    }
  }, 1000)
};

export const setGameTimer = (room, time, playTime, text, socket) => {
  const textContainer = getHTMLElement('#text-container');
  let counter = time;

  disableRoomButtons(time, textContainer);

  const interval = setInterval(() => {
    textContainer.innerText = counter - 1;
    counter -= 1;
  }, 1000);

  setTimeout(() => {
    clearInterval(interval);
    textContainer.style.fontSize = '1em';
    textContainer.innerHTML = `<span class="next-letter">${text[0]}</span>` + text.slice(1);

    startGame(room, playTime, text, textContainer, socket);
  }, time * 1000);
}
import { replacePages, getHTMLElement } from "../helpers/game.mjs";
import { CREATE_ROOM, JOIN_TO_ROOM, LEAVE_ROOM, READY  } from "../socket/env.mjs";


// JoinButton block
const onJoinButtonClick = (ev, socket) => {
  const dataset = ev.target.dataset;

  if (dataset.roomName) {
    socket.emit(JOIN_TO_ROOM, dataset.roomName);
  };
};

export const updateJoinButtonListeners = (socket) => {
  const buttons = document.querySelectorAll('.button.join-btn');

  if (buttons) {
    buttons.forEach(button => {
      button.addEventListener('click', (ev) => onJoinButtonClick(ev, socket));
    })
  };
};

// CreateRoom block
const onCreateRoomClick = (socket) => {
  const roomName = prompt('Please enter the name of the room', '');

  if (!(roomName.length < 4)) {
    socket.emit(CREATE_ROOM, roomName);
  } else {
    alert('The name must be at least 4 characters');
  }
};

const createRoomListener = (socket) => {
  const createRoomButton = getHTMLElement('#add-room-btn');

  if (createRoomButton) {
    createRoomButton.addEventListener('click', () => onCreateRoomClick(socket));
  };
};

// LeaveRoom block
const onLeaveRoomClick = (roomId, socket) => {
  const roomPage = getHTMLElement('#rooms-page');
  const gamePage = getHTMLElement('#game-page');

  const readyButton = getHTMLElement('#ready-btn');
  const readyButtonClone = readyButton.cloneNode(true);

  readyButton.parentNode.replaceChild(readyButtonClone, readyButton);

  if (roomId) {
    socket.emit(LEAVE_ROOM, roomId);
    replacePages(gamePage, roomPage);
  }
}

export const createLeaveRoomListener = (roomId, socket) => {
  const leaveRoomButton = getHTMLElement('#quit-room-btn');

  if (leaveRoomButton) {
    leaveRoomButton.addEventListener('click', () => onLeaveRoomClick(roomId, socket))
  }
}

// Ready block
const onReadyButtonClick = (ev, roomId, socket) => {
  const status = ev.target.innerText.toLowerCase();

  if (status === 'ready') {
    ev.target.innerText = 'Not Ready';
  } else {
    ev.target.innerText = 'Ready';
  }

  socket.emit(READY, roomId, status);
}

export const createReadyListener = (roomId, socket) => {
  const readyButton = getHTMLElement('#ready-btn');

  if (readyButton) {
    readyButton.addEventListener('click', (ev) => onReadyButtonClick(ev, roomId, socket));
  }
}

// Logout block
const handleLogout = () => {
  sessionStorage.removeItem('username');
  window.location.href = '/login';
};

const createLogoutListener = () => {
  const logoutButton = getHTMLElement('#logout');

  if (logoutButton) {
    logoutButton.addEventListener('click', handleLogout);
  }
}

export const initListeners = (socket) => {
  createRoomListener(socket);
  createLogoutListener();
}
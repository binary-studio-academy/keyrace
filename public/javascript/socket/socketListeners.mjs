import { MAXIMUM_USERS_FOR_ONE_ROOM } from "../config.mjs";
import { setGameTimer } from "../game/index.mjs";
import { 
  initListeners, 
  createLeaveRoomListener, 
  createReadyListener, 
  updateJoinButtonListeners, 
} from "../game/listeners.mjs";
import { getHTMLElement } from "../helpers/game.mjs";
import { 
  allAreReady,
  deleteCard,
  createRoom,
  createUserCard,
  getObjectSize, 
  replacePages, 
  updateGameRoom, 
  updateUser, 
  updateRoomsContainer,    
} from "../helpers/game.mjs";

export const handleOnLogin = (username, isSuccess, errorMessage = '', socket) => {
  if (!isSuccess) {
    alert(errorMessage);
    sessionStorage.removeItem('username');
    window.location.href = '/login';
  } else {
    updateUser(username);
    initListeners(socket);
  };
};

export const handleReceiveRooms = (rooms, deletedUser, socket) => {
  const roomsEntries = Object.entries(rooms);

  if (deletedUser) {
    return deleteCard(deletedUser);
  }

  if (roomsEntries.length > 0) {
    const processedRooms = roomsEntries.map(roomData => {
      const allReady = allAreReady([...roomData]);

      if (getObjectSize(roomData[1]) < MAXIMUM_USERS_FOR_ONE_ROOM && !allReady) {
        return createRoom(roomData[0], getObjectSize(roomData[1]));
      };
      
      return '';
    });

    updateRoomsContainer(processedRooms);
    updateJoinButtonListeners(socket);
  } else {
    updateRoomsContainer([], true);
  };
};

export const handleJoinToRoom = (username, roomId, room, socket) => {
  const roomPage = getHTMLElement('#rooms-page');
  const gamePage = getHTMLElement('#game-page');

  updateGameRoom(username, roomId, room);
  replacePages(roomPage, gamePage);

  createLeaveRoomListener(roomId, socket);
  createReadyListener(roomId, socket);
}

export const handleUsersUpdated = (user, room, disconnectedUser) => {
  const usersContainer = getHTMLElement('.users');

  if (disconnectedUser) {
    deleteCard(disconnectedUser);
  } else {
    const roomEntries = Object.entries(room);
    const updatedCards = roomEntries.map(userData => createUserCard(user, userData[0], userData[1] === 'ready'));
  
    if (usersContainer) {
      usersContainer.innerHTML = updatedCards.join('');
    }
  }
}

export const handleGameStarted = async (room, time, playTime, textId, socket) => {
  await fetch(`http://localhost:3002/game/texts/${textId}`)
          .then((res) => res.json())
          .then((json) => setGameTimer(room, time, playTime, json, socket));
}

export const handleOnProgress = (username, percent) => {
  const userProgressBar = getHTMLElement(`.user-progress.${username}`);

  if (userProgressBar) {
    userProgressBar.style.width = `${Math.floor(percent)}%`;

    if (percent === 100) {
      userProgressBar.classList.add('ready-status-success');
    }
  }
}

export const handleShowMessage = (message) => {
  alert(message);
};
import { createClientSocketActions } from "../socket/socket.mjs";

export const createSocket = (username) => {
  const socket = io("", { query: { username } });
  const socketActions = createClientSocketActions(username, socket);

  socketActions.onLogin();
  socketActions.onRooms();
  socketActions.onJoin();
  socketActions.onUsersUpdated();
  socketActions.onGameStarted();
  socketActions.onProgress();
  socketActions.onGameOver();
  socketActions.onError();
}
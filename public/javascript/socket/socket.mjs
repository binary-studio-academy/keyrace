import { handleGameOver } from "../game/index.mjs";
import { LOGIN, JOIN, ROOMS, ERROR, USERS_UPDATED, GAME_STARTED, PROGRESS, GAME_OVER } from "./env.mjs";
import { 
  handleOnLogin, 
  handleReceiveRooms, 
  handleJoinToRoom, 
  handleShowMessage, 
  handleUsersUpdated,
  handleGameStarted,
  handleOnProgress
} from "./socketListeners.mjs";

export const createClientSocketActions = (username, socket) => {
  return {
    onLogin: function() {
      return socket.on(LOGIN, (status, errorMessage) => handleOnLogin(username, status, errorMessage, socket));
    },

    onRooms: function() {
      return socket.on(ROOMS, (rooms, deletedUser) => handleReceiveRooms(rooms, deletedUser, socket));
    },

    onJoin: function() {
      return socket.on(JOIN, (roomId, room) => handleJoinToRoom(username, roomId, room, socket));
    },

    onUsersUpdated: function() {
      return socket.on(USERS_UPDATED, (room, disconnectedUser) => handleUsersUpdated(username, room, disconnectedUser));
    },

    onGameStarted: function() {
      return socket.on(GAME_STARTED, (room, time, playTime, textId) => handleGameStarted(room, time, playTime, textId, socket));
    },

    onProgress: function() {
      return socket.on(PROGRESS, (username, percent) => handleOnProgress(username, percent));
    },

    onGameOver: function() {
      return socket.on(GAME_OVER, (userResults, playTime) => handleGameOver(userResults, playTime));
    },

    onError: function() {
      return socket.on(ERROR, (message) => handleShowMessage(message));
    }
  }
}
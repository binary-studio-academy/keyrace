import { PROGRESS } from "../socket/env.mjs";

export const untraceableButtons = ['Tab', 'CapsLock', 'Shift', 'Control', 'Meta', 'Alt'];

export const getHTMLElement = (selector) => document.querySelector(selector); 

export const allAreReady = (data) => {
  data.shift(); // room name

  return data.every(user => Object.values(user)[0] === "ready");
}

export const deleteCard = (username) => {
  const user = getHTMLElement(`li[data-username=${username}]`);

  if (user) {
    user.remove();
  }
} 

export const updateUser = (username) => {
  const userNameContainer = getHTMLElement('#username');

  if (userNameContainer) {
    const userNameHTML = userNameContainer.innerHTML;

    userNameContainer.innerHTML = userNameHTML.includes(username) ? userNameHTML : userNameHTML + username + '!';
  };
};

export const updateRoomsContainer = (processedRooms, isEmpty = false) => {
  const roomsContainer = getHTMLElement('#rooms');

  if (isEmpty) {
    roomsContainer.innerHTML = '';
  } else if (roomsContainer) {
    roomsContainer.innerHTML = processedRooms.join('');
  };
};

export const replacePages = (currentPage, newPage) => {
  if (currentPage && newPage) {
    currentPage.classList.add('display-none');
    newPage.classList.remove('display-none');
  };
};

export const getObjectSize = (object) =>  {
  let size = 0;

  for (const key in object) {
    if (object.hasOwnProperty(key)) {
      size += 1;
    };
  }
  return size;
};

export const createRoom = (roomName, roomCount) => {
  return (`
    <article class="room">
      <p class="room__user">${roomCount} users connected</p>
      <div class="room__info">
        <h4>${roomName}</h4>
        <button class="button join-btn" data-room-name="${roomName}">Join</button>
      </div>
    </article>
  `);
};

export const createUserCard = (userName, user, isReady) => {
  const userTitle = userName === user ? `${user} (you)` : user;

  return (`
    <li class="user" data-username="${user}">
      <span class="user-wrapper">
        <i class="user-ready-status 
          ${isReady ? "ready-status-green" : "ready-status-red"}">
        </i>
        <h5 class="user-name">${userTitle}</h5>
        <div class="progress-bar">
          <div class="user-progress ${user}"></div>
        </div>
      </span>
    </li>
  `);
}

const createSideBar = (roomName, users) => {
  return (`
    <div class="game-info">
      <h2 class="game-title">${roomName}</h2>
      <button data-room-name="${roomName}" id="quit-room-btn" class="button">Back To Rooms</button>
    </div>
    <ul class="users">
      ${users}
    </ul>
  `);
}

export const updateGameRoom = (user, roomName, room) => {
  const gameSidebar = getHTMLElement('.game-sidebar');

  const roomEntries = Object.entries(room);
  const userCards = roomEntries.map(userData => createUserCard(user, userData[0], userData[1] === 'ready'));

  if (gameSidebar) {
    gameSidebar.innerHTML = createSideBar(roomName, userCards.join(''));
  }
}

export const refreshGame = () => {
  const leaveRoomButton = getHTMLElement('#quit-room-btn');
  const textContainer = getHTMLElement('#text-container');
  const readyButton = getHTMLElement('#ready-btn');
  const progressBars = document.querySelectorAll('.user-progress');

  if (leaveRoomButton) {
    leaveRoomButton.style.display = "block";
  }

  if (textContainer) {
    textContainer.innerHTML = '';
    textContainer.style = '';
  }

  if (readyButton) {
    readyButton.style = '';
    readyButton.click();
  }

  if (progressBars.length) {
    progressBars.forEach(bar => {
      bar.classList.remove('ready-status-success');
      bar.style = '';
    })
  }
}

const getChartersPerMin = (mistakes, index, time, playTime) => {
  if (time !== "You haven't finished typing") {
    return Math.floor((mistakes + index) / (playTime - time) * 60);
  } else {
    return Math.floor((mistakes + index) / playTime * 60);
  }
}

const getTeamsResultHTML = (playTime, results) => {
  const username = sessionStorage.getItem('username');
  const htmlResults = results.map((el, index) => (`
    <li class="results-item">
      <span id="place-${index + 1}">#${index + 1} ${el[0] === username ? `${username} (you)` : `${el[0]}`}</span>
      <span>${playTime - Number(el[1])}s</span>
    </li>
  `));

  return htmlResults.join('');
}

export const setResults = (playTime, userResults) => {
  const time = sessionStorage.getItem('time') || "You haven't finished typing";
  const mistakes = sessionStorage.getItem('mistake') || 0;
  const index = sessionStorage.getItem('index') || 0;
  
  const chartersPerMin = index ? 
    getChartersPerMin(Number(mistakes), Number(index), Number(time), Number(playTime)) : 0;
  const teamResults = getTeamsResultHTML(playTime, userResults);

  setSessionValues(true);

  return (`
    <div class="results-wrapper">
      <h3 class="results-title">Results</h3>
      <ul class="results-list">
        <li class="results-item">
          <span>Time: </span>
          <span>${Number(playTime) - Number(time)}s</span>
        </li>
        <li class="results-item">
          <span>Characters per minute: </span>
          <span>~${chartersPerMin}</span>
        </li>
        <li class="results-item">
          <span>Mistakes: </span>
          <span>${mistakes}</span>
        </li>
      </ul>
      <ul class="results-list">
        <h4>Room results</h4>
        ${teamResults}
      </ul>
      <button id="quit-results-btn" type="button" class="button">Continue</button>
    </div>
  `)
};

export const checkLetter = (letters, index, button) => {
  if (letters && letters[index] === button) {
    return "true";
  } else {
    return "false";
  };
}

const updateTextContainerWithProgress = (container, text, newIndex) => {
  container.innerHTML = 
  `<span class="correct">${text.slice(0, newIndex)}</span>` + 
  `<span class="next-letter">${text[newIndex]}</span>` + 
  `<span>${text.slice(newIndex + 1)}</span>`;
}

export const successLetter = (textContainer, text, letters, newIndex, room, socket) => {
  updateTextContainerWithProgress(textContainer, text, newIndex);

  const prevPercent = sessionStorage.getItem('percent');
  const percent = Math.floor(newIndex / (letters.length / 100));

  if (prevPercent < percent) {
    sessionStorage.setItem('percent', percent);
    socket.emit(PROGRESS, room, percent);
  }

  sessionStorage.setItem('index', newIndex);
}

export const handleOnWin = (text, room, textContainer, socket) => {
  const timer = getHTMLElement('#game-time');
  const currentTime = parseInt(timer.innerText);

  sessionStorage.setItem('time', currentTime);

  textContainer.innerHTML = `<span class="correct">${text}</span>`;
  
  socket.emit(PROGRESS, room, 100, currentTime);
}

export const setSessionValues = (endGame) => {
  if (endGame) {
    sessionStorage.removeItem('index');
    sessionStorage.removeItem('mistake');
    sessionStorage.removeItem('time');
    sessionStorage.removeItem('percent');
  }
  sessionStorage.removeItem('gameOver');
  sessionStorage.setItem('index', 0);
  sessionStorage.setItem('mistake', 0);
  sessionStorage.setItem('time', 0);
  sessionStorage.setItem('percent', 0);
}

export const disableRoomButtons = (time, container) => {
  const leaveRoomButton = getHTMLElement('#quit-room-btn');
  const readyButton = getHTMLElement('#ready-btn');

  if (leaveRoomButton) {
    leaveRoomButton.style.display = 'none';
  }

  if (readyButton) {
    readyButton.style.display = 'none';
  }

  if (container) {
    container.style.display = 'block';
    container.innerText = time;
  }
}

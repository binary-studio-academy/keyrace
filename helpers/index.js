import { texts } from "../data";
import { MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME } from "../socket/config";
import { GAME_OVER, GAME_STARTED, ROOMS, USERS_UPDATED } from "../socketEnv";

export const isRoomAvailable = (username, roomId, room, inGame) => room && !room[username] && !inGame[roomId] && getObjectLength(room) < MAXIMUM_USERS_FOR_ONE_ROOM;

export const leaveFromRooms = (user, socket) => {
  user.forEach(room => socket.leave(room));
  user = [];
}

export const isUserExist = (place, roomId, username) => place[roomId] && place[roomId][username];

export const deleteInGameData = (inGame, roomId, username) => {
  if (getObjectLength(inGame[roomId] <= 2)) {
    delete inGame[roomId];
  } else {
    delete inGame[roomId][username];
  }
}

export const getObjectLength = (object) =>  {
  let length = 0;

  for (const key in object) {
    if (object.hasOwnProperty(key)) {
      length += 1;
    };
  }
  return length;
};

export const getRandomTextId = () => {
  const random = Math.random() * texts.length;
  return Math.floor(random);
}

export const updateUsersStatus = (roomId, room, rooms, inGame, socket, onLeave = false) => {
  const roomEntries = Object.entries(room);
  const allAreReady = roomEntries.every(el => el[1] === 'ready');

  if (!onLeave) {
    socket.emit(USERS_UPDATED, room);
  }

  socket.to(roomId).emit(USERS_UPDATED, room);

  if (allAreReady) {
    const textId = getRandomTextId();
    inGame[roomId] = {
      roomId,
    }

    roomEntries.forEach(user => {
      inGame[roomId] = {
        ...inGame[roomId],
        [user[0]]: false,
      };
    })

    socket.broadcast.emit(ROOMS, rooms);
    socket.emit(GAME_STARTED, roomId, SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME, textId);
    socket.broadcast.to(roomId).emit(GAME_STARTED, roomId, SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME, textId);
  }
}

const checkIsOver = (roomUsers, inGameUsers, deletedUser) => inGameUsers.every(user => {
  if (user[0] !== deletedUser && roomUsers.includes(user[0])) {
    return typeof user[1] === 'number'; // true if game end, false if game in process
  }

  return true;
});

const getResults = (inGameUsers) => {
  const finishedUsers = inGameUsers.filter(user => typeof user[1] === 'number');

  return finishedUsers.length > 1 && finishedUsers.sort((a, b) => b[1] - a[1]) || finishedUsers;
}

const resetUsers = (room) => {
  const updatedUsers = Object.entries(room).map(el => [el[0], 'not-ready']);
  
  return Object.fromEntries(updatedUsers);
}

export const updateInGameUsers = (users) => {
  const usersEntries = Object.entries(users);
  const updatedUsers = usersEntries.map((user, index) => {
    if (index === 0) {
      return user; // room name
    }

    return [user[0], typeof user[1] === 'number' ? user[1] : 0];
  });
  
  return Object.fromEntries(updatedUsers);
}

export const checkIsWin = (roomId, rooms, inGame, socket, deletedUser = '') => {
  if (rooms[roomId] && inGame[roomId]) {
    const roomUsers = Object.entries(rooms[roomId]).map(user => user[0]);
    const inGameEntries = Object.entries(inGame[roomId]);
  
    inGameEntries.shift(); // remove the name of the room

    const isOver = checkIsOver(roomUsers, inGameEntries, deletedUser);
  
    if (isOver) {
      inGame[roomId] = undefined;
      rooms[roomId] = resetUsers(rooms[roomId]);

      const userResults = getResults(inGameEntries);

      socket.emit(GAME_OVER, userResults, SECONDS_FOR_GAME);
      socket.to(roomId).broadcast.emit(GAME_OVER, userResults, SECONDS_FOR_GAME);
    }
  }
}
import { createSocketActions } from "./socket";
import { CREATE_ROOM, GAME_OVER, JOIN_TO_ROOM, LEAVE_ROOM, PROGRESS, READY } from "../socketEnv";

const users = {

};

const rooms = {
  
};

const inGame = {

}

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    const socketActions = createSocketActions(rooms, username, users, inGame, socket);

    socketActions.login();

    socket.on(CREATE_ROOM, (roomId) => socketActions.createRoom(roomId));
    socket.on(JOIN_TO_ROOM, (roomId) => socketActions.joinToRoom(roomId));
    socket.on(READY, (roomId, status) => socketActions.onReady(roomId, status));
    socket.on(PROGRESS, (roomId, percent, time) => socketActions.onProgress(roomId, percent, time));
    socket.on(GAME_OVER, (roomId) => socketActions.onGameOver(roomId));
    socket.on(LEAVE_ROOM, (roomId) => socketActions.leaveRoom(roomId));
    socket.on('disconnect', () => socketActions.disconnectUser());
  });
};
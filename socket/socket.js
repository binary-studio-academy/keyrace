import { checkIsWin, getObjectLength, updateUsersStatus, isRoomAvailable, isUserExist, deleteInGameData, updateInGameUsers } from "../helpers";
import { ERROR, JOIN, LOGIN, ROOMS, USERS_UPDATED, PROGRESS } from "../socketEnv";

export const createSocketActions = (rooms, username, users, inGame, socket) => {
  return {
    login: function() {
      if (users[username]) {
        socket.emit(LOGIN, false, 'Name is already taken');
      } else {
        users[username] = [];
    
        socket.emit(LOGIN, true);
        socket.emit(ROOMS, rooms);
      };
    },

    createRoom: function(roomId) {
      if (!rooms[roomId]) {
        socket.join(roomId);

        rooms[roomId] = { [username]: 'not-ready' };
        users[username].push(roomId);
      
        socket.emit(JOIN, roomId, rooms[roomId]);
        socket.broadcast.emit(ROOMS, rooms);
      } else {
        socket.emit(ERROR, 'A room with this name already exists');
      }
    },

    joinToRoom: function(roomId) {
      const roomAvailable = isRoomAvailable(username, roomId, rooms[roomId], inGame);

      if (roomAvailable) {

        if (users[username]?.length) {
          leaveFromRooms(users[username], socket);
        }

        socket.join(roomId);
    
        rooms[roomId][username] = 'not-ready';
        users[username].push(roomId);
    
        socket.emit(JOIN, roomId, rooms[roomId]);
        socket.to(roomId).emit(USERS_UPDATED, rooms[roomId]);
        socket.broadcast.emit(ROOMS, rooms);
      } else if (inGame[roomId]) {
        socket.emit(ERROR, 'Game already start');
      } else {
        socket.emit(ERROR, 'The room has the maximum number of users');
      };
    },

    onReady: function(roomId, status) {
      if (rooms[roomId]) {
        rooms[roomId][username] = status;

        updateUsersStatus(roomId, rooms[roomId], rooms, inGame, socket);
      }
    },

    onProgress: function(roomId, percent, time = 0) {
      if (percent === 100) {
        inGame[roomId] = {
          ...inGame[roomId],
          [username]: time,
        }
        
        checkIsWin(roomId, rooms, inGame, socket);
      }

      socket.emit(PROGRESS, username, percent);
      socket.to(roomId).broadcast.emit(PROGRESS, username, percent);
    },

    onGameOver: function(roomId) {
      if (inGame[roomId]) {
        inGame[roomId] = updateInGameUsers(inGame[roomId]);

        checkIsWin(roomId, rooms, inGame, socket);
      }
    },

    leaveRoom: function(roomId) {
      if(isUserExist(rooms, roomId, username)) {
        socket.leave(roomId);
        delete rooms[roomId][username];

        if (isUserExist(inGame, roomId, username)) {
          deleteInGameData(inGame, roomId, username);
        }

        if (!getObjectLength(rooms[roomId])) {
          delete rooms[roomId];
        } else {
          updateUsersStatus(roomId, rooms[roomId], rooms, inGame, socket, true);
        }
      }

      socket.emit(ROOMS, rooms);
      socket.broadcast.emit(ROOMS, rooms);
    },

    disconnectUser: function() {
      if (users[username]?.length > 0) {
        users[username].forEach(room => {
          checkIsWin(room, rooms, inGame, socket, username);

          if (isUserExist(rooms, room, username)) {
            delete rooms[room][username];

            socket.to(room).emit(USERS_UPDATED, rooms[room], username);
          }

          if (isUserExist(inGame, room, username)) {
            deleteInGameData(inGame, room, username);
          }

          if (!getObjectLength(rooms[room])) {
            delete rooms[room];
          }
        });

        delete users[username];
    
        socket.broadcast.emit(ROOMS, rooms);
        socket.disconnect();
      } else if (users[username]) {
        delete users[username];
    
        socket.disconnect();
      };
    },
  };
};